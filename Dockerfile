FROM node:15
WORKDIR /app
COPY package.json .
COPY package-lock.json .
RUN npm install
RUN ls
COPY . .
RUN ls
RUN npm run build
EXPOSE 3000
CMD ["npm" ,"start"]
